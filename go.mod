module github.com/kamilsk/check

go 1.11

require (
	github.com/PuerkitoBio/goquery v1.5.1 // indirect
	github.com/antchfx/htmlquery v1.2.2 // indirect
	github.com/antchfx/xmlquery v1.2.3 // indirect
	github.com/antchfx/xpath v1.1.4 // indirect
	github.com/briandowns/spinner v1.9.0
	github.com/fatih/color v1.9.0
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/gocolly/colly v1.2.0
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/pkg/errors v0.9.1
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	github.com/spf13/cobra v0.0.6
	github.com/stretchr/testify v1.5.1
	github.com/temoto/robotstxt v1.1.1 // indirect
	go.octolab.org/toolkit/cli v0.0.7
)
